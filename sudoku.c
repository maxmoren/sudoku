/*
  sudoku.c - sudoku solver
  Max Morén <maxmoren@gmail.com>
  2010-01-16

  The solver reads the puzzle from stdin as rows.
  1-9 is hints, '.' or 0 is empty.

 */

#include <stdio.h>
#include <ctype.h>

#define Y(i) (i / 9)
#define X(i) (i % 9)

int x[9][9] = { { 0 } };
int y[9][9] = { { 0 } };
int subs[3][3][9] = { { { 0 } } };
int grid[81] = { 0 };
int ro[81] = { 0 };

void print_grid(void)
{
	int x, y;
	char c;

	for (y = 0; y < 9; y++)
	{
		for (x = 0; x < 9; x++)
		{
			c = grid[x+y*9] + '0';

			putchar(c == '0' ? '.' : c);
			putchar(' ');
		}

		putchar('\n');
	}
}

void set(int i, int n)
{
	grid[i] = n;

	x[X(i)][n] = y[Y(i)][n] = 1;
	subs[(X(i)/3)][(Y(i)/3)][n] = 1;
}

void unset(int i, int n)
{
	if (n == 0)
		return;

	grid[i] = 0;

	x[X(i)][n] = y[Y(i)][n] = 0;
	subs[(X(i)/3)][(Y(i)/3)][n] = 0;
}

inline int isset(int i, int n)
{
	return y[Y(i)][n] || x[X(i)][n] || subs[(X(i)/3)][(Y(i)/3)][n];
}

int guess(int i)
{
	int n, n0 = grid[i];

	for (n = n0 + 1; n <= 9; n++)
		if (!isset(i, n))
		{
			unset(i, n0);
			set(i, n);

			return 1;
		}

	unset(i, n0);

	return 0;
}

int main(void)
{
	int c, i = 0;

	while ((c = getchar()) != EOF && i < 81)
		if (isdigit(c) || c == '.')
		{
			if (c != '0' && c != '.')
			{
				set(i, c - '0');
				ro[i] = 1;
			}

			++i;
		}

	for (i = 0; i >= 0 && i < 81;)
	{
		if (ro[i])
		{
			++i;
			continue;
		}

		if (guess(i))
			++i;
		else
		{
			--i;
			while (ro[i] && i >= 0)
				--i;
		}
	}

	print_grid();

	return 0;
}

